using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using RestCore.Models;

namespace RestCore.Controllers;

public class TodoContext : DbContext
{
    public TodoContext(DbContextOptions<TodoContext> options) : base(options)
    {
    }

    public DbSet<TodoItem> TodoItems { get; set; } = null!;
}